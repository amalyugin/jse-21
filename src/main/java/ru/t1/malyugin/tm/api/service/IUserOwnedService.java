package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.api.repository.IUserOwnerRepository;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnerRepository<M> {

    List<M> findAll(String userId, Sort sort);

}
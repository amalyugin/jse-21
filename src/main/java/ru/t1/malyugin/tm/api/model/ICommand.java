package ru.t1.malyugin.tm.api.model;

import ru.t1.malyugin.tm.enumerated.Role;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute();

    Role[] getRoles();

}
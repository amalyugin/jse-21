package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    int getSize(String userId);

    M add(String userId, M model);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

}
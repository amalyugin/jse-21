package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
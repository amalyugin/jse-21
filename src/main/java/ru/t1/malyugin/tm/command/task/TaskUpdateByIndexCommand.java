package ru.t1.malyugin.tm.command.task;

import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "task-update-by-index";

    private static final String DESCRIPTION = "Update task by index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }

}
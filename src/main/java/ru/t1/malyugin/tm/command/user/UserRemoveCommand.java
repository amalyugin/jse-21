package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.enumerated.Role;

public final class UserRemoveCommand extends AbstractUserCommand {

    private static final String NAME = "user-remove";

    private static final String DESCRIPTION = "remove current user profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE CURRENT USER]");
        final String userId = getAuthService().getUserId();
        getUserService().removeById(userId);
        getAuthService().logout();
    }

}
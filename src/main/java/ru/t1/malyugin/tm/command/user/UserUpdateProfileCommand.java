package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String NAME = "user-update";

    private static final String DESCRIPTION = "update current user profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        final String userId = getAuthService().getUserId();
        System.out.print("ENTER FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.print("ENTER MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        System.out.print("ENTER LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
package ru.t1.malyugin.tm.model;

import ru.t1.malyugin.tm.api.model.IWBS;
import ru.t1.malyugin.tm.enumerated.Status;

import java.util.Date;

public abstract class AbstractWBSModel extends AbstractUserOwnedModel implements IWBS {

    protected Date created = new Date();

    protected Status status = Status.NOT_STARTED;

    protected String name = "";

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

}
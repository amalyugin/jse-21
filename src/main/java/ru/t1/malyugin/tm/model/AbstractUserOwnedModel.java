package ru.t1.malyugin.tm.model;

public abstract class AbstractUserOwnedModel extends AbstractModel {

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
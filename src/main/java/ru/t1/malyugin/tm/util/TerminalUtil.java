package ru.t1.malyugin.tm.util;

import org.apache.commons.lang3.math.NumberUtils;
import ru.t1.malyugin.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInteger() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final RuntimeException e) {
            throw new NumberIncorrectException(value, e);
        }
    }

    static Integer nextIntegerSafe() {
        final String value = nextLine();
        if (NumberUtils.isParsable(value)) return Integer.parseInt(value);
        return null;
    }

}
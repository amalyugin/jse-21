package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IUserOwnerRepository;
import ru.t1.malyugin.tm.api.service.IUserOwnedService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public void clear(final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        repository.clear(userId.trim());
    }

    @Override
    public int getSize(final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return repository.getSize(userId.trim());
    }

    @Override
    public M add(final String userId, final M model) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.add(userId.trim(), model);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(userId.trim(), model);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        M model = repository.removeById(userId.trim(), id.trim());
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        M model = repository.removeByIndex(userId.trim(), index);
        if (model == null) throw new EntityNotFoundException();
        return model;

    }

    @Override
    public List<M> findAll(final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return repository.findAll(userId.trim());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll();
        return repository.findAll(userId.trim(), comparator);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) return null;
        return repository.findOneById(userId.trim(), id.trim());
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId.trim(), index);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (sort == null) return repository.findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId.trim(), comparator);
    }

}
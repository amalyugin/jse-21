package ru.t1.malyugin.tm.exception.field;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value) {
        super("Error! This number '" + value + "' is incorrect...");
    }

    public NumberIncorrectException(final String value, final Throwable cause) {
        super("Error! This number '" + value + "' is incorrect...");
    }

}